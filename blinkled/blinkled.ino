int led_pin = 13;
int blink = 0;
unsigned long myTime;
unsigned long newTime;

void setup() {
  // put your setup code here, to run once:
  pinMode(led_pin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  blink_led(blink);

  Serial.print("Time: ");
  myTime = millis();

  Serial.println(myTime); // prints time since program started
  if(myTime > 100000){
    Serial.end();
  }
  if(newTime = myTime % 2){
    blink = 200;
  }
  else{
    blink = 400;
  }
  Serial.print("New Time: ");
  Serial.println(newTime);
  delay(400);          // wait a second so as not to send massive amounts of data
}

void blink_led(int bt){
  digitalWrite(13,HIGH);
  delay(bt);
  digitalWrite(13,LOW);
  delay(bt);

}
